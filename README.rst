manywindows
===========

This Docker image is intended as a windows version of the pypi manylinux 
docker image, useful for building & testing python modules on a variety 
of python versions under windows.

The standard 'py' launcher should be available on the path and can be used 
to specify the desired version of python to run.
Otherwise each of the available python installations are availble directly
on C:\Python-<version>(-amd64)