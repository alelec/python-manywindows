[CmdletBinding(DefaultParameterSetName="Standard")]

 param (
    [Parameter(Mandatory=$true)]
    [string]
    $Version
 )

$PackageDir = Split-Path $script:MyInvocation.MyCommand.Path
mkdir temp -Force

$python = "C:\Python-$Version\python.exe"
Write-Host "$python"
if (Test-Path $python) {
    Write-Host "Already installed: $python"; 
} else {

    $vers_parts = $Version.split('.')
    $vers_major = [convert]::ToInt32($vers_parts[0], 10)
    $vers_minor = [convert]::ToInt32($vers_parts[1], 10)
    $vers_revis = 0
    if ($vers_parts.Count -gt 2) {
        $vers_revis = [convert]::ToInt32($vers_parts[2].split('-')[0], 10)
    }
    
    $new_installers = ($vers_major -ge 3) -and ($vers_minor -ge 5)

    $InstallerSuffix = (&{If($new_installers) {"exe"} Else {"msi"}}) 
    $Installer = "python-$Version.$InstallerSuffix"

    if (-not (Test-Path $Installer)) {
        Write-Host "Downloading $Version"; 
        $BaseVersion = $Version.Split("-")[0]
        
        # Python2 x64 installers have .amd64 rather than -amd64
        $urlvers = (&{If($new_installers) { $Version } Else { $Version -replace "-", '.' }})
        
        $url = "https://www.python.org/ftp/python/$BaseVersion/python-$urlvers.$InstallerSuffix" 
        
        Write-Host "Downloading $url ..."
        [Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
        # wget -UseBasicParsing $url -Outfile $Installer 
        aria2c -o $Installer $url
    }

    Write-Host "Installing $Version ..."; 

    if ($InstallerSuffix -eq 'exe') {
        # https://docs.python.org/3.5/using/windows.html#installing-without-ui
        Start-Process $Installer -Wait -ArgumentList @( 
                '/passive', 
                'InstallAllUsers=1', 
                "TargetDir=C:\Python-$Version", 
                'PrependPath=0', 
                'Shortcuts=0', 
                'Include_doc=0', 
                'Include_test=0',
                'InstallLauncherAllUsers=1'
            ); 

    } else {
        # https://www.python.org/download/releases/2.5/msi/
        Start-Process msiexec -Wait -ArgumentList @( 
                '/i', $Installer,
                '/quiet',
                '/qb',
                "TARGETDIR=C:\Python-$Version",
                'ALLUSERS=1',
                'ADDLOCAL=All',
                'REMOVE=Extensions,PrependPath'
            );
    }


    ## the installer updated PATH, so we should refresh our local value
    #$env:PATH = [Environment]::GetEnvironmentVariable('PATH', [EnvironmentVariableTarget]::Machine); 

    Write-Host 'Verifying install ...'; 
    Write-Host "  $python --version"; &$python --version

    #$getpip = "$PackageDir\get-pip.py"
    #if (-not (Test-Path $getpip)) {
    #   Write-Host ('Installing/Upgrading pip ...'); 
    #   (New-Object System.Net.WebClient).DownloadFile('https://bootstrap.pypa.io/get-pip.py', $getpip); 
    #}
    #&$python $getpip

    Write-Host 'Updating pip install ...'; 
    &$python -m pip install -U pip setuptools wheel pipenv win-unicode-console setuptools_scm;
}

Write-Host 'Complete.';
