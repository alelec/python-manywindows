FROM mcr.microsoft.com/windows/servercore:1809

SHELL ["powershell", "-executionpolicy", "bypass", "-Command", "$ErrorActionPreference = 'Stop';"]

COPY Install-Python.ps1 Install-Python.ps1

ENV PYTHONIOENCODING=UTF-8

RUN iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

# Git is required for use with Gitlab CI
RUN choco install -y git.install -params '"/GitAndUnixToolsOnPath"'

# Compiler for python 2.6 - 3.2
# choco dotnet3.5 fails, install dependencies manually
RUN choco install -y vcredist2008
# ref. https://blogs.technet.microsoft.com/mniehaus/2015/08/31/adding-features-including-net-3-5-to-windows-10/
#RUN Add-WindowsCapability -Online -Name NetFx3~~~~
RUN choco install -y --ignoredependencies vcpython27

# Compiler for python 3.3 - 3.4
RUN choco install -y windows-sdk-7.1

# Compiler for python 3.5 - 3.7
RUN choco install -y vcbuildtools
RUN choco install -y visualcpp-build-tools

# Add the rest of MS Visual Studio 15 build tools
RUN choco install -y microsoft-build-tools

# Install aria2 download util
RUN choco install -y aria2

# Let the snakes loose
RUN ./Install-Python.ps1 -Version 3.7.2
RUN ./Install-Python.ps1 -Version 3.7.2-amd64
RUN ./Install-Python.ps1 -Version 3.6.8
RUN ./Install-Python.ps1 -Version 3.6.8-amd64
# Last Windows build of 3.5 is 3.5.4
RUN ./Install-Python.ps1 -Version 3.5.4
RUN ./Install-Python.ps1 -Version 3.5.4-amd64

## msi installs are currently failing
#RUN ./Install-Python.ps1 -Version 3.4.4
#RUN ./Install-Python.ps1 -Version 3.4.4-amd64
#RUN ./Install-Python.ps1 -Version 2.7.13
#RUN ./Install-Python.ps1 -Version 2.7.13-amd64

RUN ./Install-Python.ps1 -Version 2.7.16
RUN ./Install-Python.ps1 -Version 2.7.16-amd64


# Add WIX Toolset for building installers (used with briefcase)
ADD https://github.com/wixtoolset/wix3/releases/download/wix311rtm/wix311-binaries.zip c:\\wix311.zip
RUN mkdir c:\\wix311;Expand-Archive -Path c:\\wix311.zip -DestinationPath c:\\wix311\\bin
ENV WIX "C:\\wix311\\"

# Add python 3.7 to the path, provides easy access to pipenv
RUN $newPath='C:\Python-3.7.2-amd64\Scripts;' + $env:PATH; \
    Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH –Value $newPath

CMD ["powershell"]